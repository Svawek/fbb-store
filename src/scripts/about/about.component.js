'use strict';

angular.
    module('about').
    component('about', {
        templateUrl: 'scripts/about/about.template.html'
    })