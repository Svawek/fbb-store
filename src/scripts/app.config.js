'use strict';

angular.
    module('bookApp').
    config(['$locationProvider', '$routeProvider',
        function config($locationProvider, $routeProvider) {
            $locationProvider.hashPrefix('!');

            $routeProvider.
                when('/books', {
                    template: '<book-list></book-list>'
                }).
                when('/books/:bookId', {
                    template: '<book-detail></book-detail>'
                }).
                when('/about', {
                    template: '<about></about>' 
                }).
                when('/books/:bookId/order', {
                    template: '<book-order></book-order>'
                }).
                otherwise('/books');
        }
    ]);