'use strict';

angular.
    module('bookList').
    component('bookList',{
        templateUrl: 'scripts/book-list/book-list.template.html',
        controller: ["$http", function bookListCtrl($http){
            var self = this;
            self.itemsDelta = 4;
            self.itemsLimit = self.itemsDelta;
            $http.get('https://netology-fbb-store-api.herokuapp.com/book').then(function(response) {
                self.books = response.data;
                self.numItems = self.books.length;
            });
            self.moreClick = function(){
              self.itemsLimit += self.itemsDelta;
            };
        }]
    });
