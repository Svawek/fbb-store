'use strict';

angular.module('bookApp', [
    'ngRoute',
    'ngSanitize',
    'ngMessages',
    'bookList',
    'bookDetail',
    'bookOrder',
    'about'
]);