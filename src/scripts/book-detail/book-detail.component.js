'use strict';

angular.
    module('bookDetail').
    component('bookDetail', {
        templateUrl: 'scripts/book-detail/book-detail.template.html',
        controller: ['$http', '$routeParams',
            function BookDetailCtrl($http, $routeParams) {
                var self = this;

                $http.get('https://netology-fbb-store-api.herokuapp.com/book/' + $routeParams.bookId).then(function(response) {
                    self.book = response.data;
                });

                
            }
        ]
    });

	$(document).ready(function(e) {

	  var moveSpaceX = 120;
	  var moveSpaceY = 110;

	  $(window).mousemove(function(e) {

	    var mX = e.pageX - $('.eye').offset().left - $('.eye').width() / 2;
	    var mY = e.pageY - $('.eye').offset().top - $('.eye').height() / 2;

	    var maxMouseX = $(window).width();
	    var maxMouseY = $(window).height();

	    var xF = mX / maxMouseX;
	    var yF = mY / maxMouseY;

	    $(".iris").css({
	      "left": moveSpaceX * xF + $(".eye-single").width() / 2 + 27,
	      "top": moveSpaceX * yF + $(".eye-single").height() / 2 + 15

	    });

	  });

	});