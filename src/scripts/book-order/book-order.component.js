'use strict';

angular.
    module('bookOrder').
    component('bookOrder', {
        templateUrl: 'scripts/book-order/book-order.template.html',
        controller: ['$http', '$routeParams', 
            function BookOrderCtrl($http, $routeParams) {
                var self = this;
                self.shipping = 0;

                $http.get('https://netology-fbb-store-api.herokuapp.com/book/' + $routeParams.bookId).then(function(response) {
                    self.book = response.data;
                });
                $http.get('https://netology-fbb-store-api.herokuapp.com/order/delivery').then(function(response) {
                    self.deliveries = response.data;
                });
                $http.get('https://netology-fbb-store-api.herokuapp.com/order/payment').then(function(response) {
                    self.payments = response.data;
                });

                self.SendData = function() {
                    console.log(1);
                    var content = angular.element(document.querySelector('.content'));
                    var succesMessage = angular.element(document.querySelector('.succes-order'));
                    var errorMessage = angular.element(document.querySelector('.error-order'))
                    var deliveryId = angular.element(document.querySelector('input[name="delivery"].ng-valid-parse')).attr('id');
                    console.log(deliveryId);
                    var data = {
                        manager: 'svawek@gmail.com',
                        book: self.book.id,
                        name: self.name,
                        phone: self.phone,
                        email: self.email,
                        comment: self.comment,
                        delivery: {
                            id: deliveryId,
                            address: self.shippingAdress,
                        },
                        payment: {
                            id: self.payment,
                            currency: 'R01235',
                        }                                               
                    };
                    console.log(self.shipping);
                    console.log(data);

                    $http({
                        method: 'POST',
                        url: 'https://netology-fbb-store-api.herokuapp.com/order',
                        data: angular.toJson(data),
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    })
                        .then(
                            function(response){
                                content = content.addClass('hide-block');
                                succesMessage = succesMessage.removeClass('hide-block');
                                errorMessage = errorMessage.addClass('hide-block');
                            },
                            function(response){
                                self.ResponseDetails = response.data.message;
                                errorMessage = errorMessage.removeClass('hide-block');
                            }
                        );
                };
            }
        ]
    });