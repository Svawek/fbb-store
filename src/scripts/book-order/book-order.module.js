'use strict';

angular.module('bookOrder', [
    'ngRoute',
    'ngSanitize',
    'ngMessages'
]);